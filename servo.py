import RPi.GPIO as GPIO
import time

class Servo(object):

    def __init__(self, servo_pin, gpio_mode = GPIO.BCM):
        
        self._servo_pin = servo_pin
        self._servo_pwm_frequency = 50
        self._stroke_mid = 7.0
        self._stroke_min = 5.5
        self._stroke_max = 8.0

        GPIO.setwarnings(False)
        self._saved_mode = GPIO.getmode()
        GPIO.setmode(gpio_mode)
        GPIO.setup(self._servo_pin, GPIO.OUT)
        self._pwm = GPIO.PWM(self._servo_pin, self._servo_pwm_frequency)
        self.last_dc = self._stroke_mid
        self._pwm.start(self.last_dc)


    def goto_min(self):
        #self._pwm.start(self.last_dc)
        for i in xrange(int(self.last_dc * 100), int(self._stroke_min * 100) - 1, -1):
            self._goto(i)
        #self._pwm.stop()
        
    def goto_max(self):
        #self._pwm.start(self.last_dc)
        for i in xrange(int(self.last_dc * 100), int(self._stroke_max * 100) + 1):
            self._goto(i)
        #self._pwm.stop()
        
    def _goto(self, dc):
        print dc
        self.last_dc = float(dc)/100
        print self.last_dc
        self._pwm.ChangeDutyCycle(self.last_dc)
        time.sleep(0.1)
