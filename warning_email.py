def send_warning_email(temp):
    import smtplib
    import logging
    from email.parser import Parser

    msg = None
    sender = 'INCUBATOR'
    to = 'einar@vading.se'
    user = 'incubator@vading.se'
    pwd = ''

    with open('email_pwd.txt', 'rb') as thepwdfile:
        pwd = thepwdfile.read()

    msg = Parser().parsestr('From: {}\n'
            'To: {}\n'
            'Subject: INCUBATOR CRITICAL TEMP ({} C)\n'
            '\n'
            'INCUBATOR FAILURE\n'.format(sender, to, temp))

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(user, pwd)
        server.sendmail(sender, to, msg.as_string())
    except:
        logging.info("Failed to send email")
