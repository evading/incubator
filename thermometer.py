import smbus
import time
import logging
import threading

class Thermometer(object):

    def __init__(self):
        
        self._threadlock = threading.Lock()

        self.bus = smbus.SMBus(0)
        self._address =       0x48
        self._temp_reg =      0x00
        self._temp_reg_len =  0x02
        self._conf_reg =      0x01
        self._conf_len =      0x01

    def get_temp(self):
        self._threadlock.acquire()

        self.bus.write_byte_data(self._address, self._conf_reg, 0x60) # 0x60, 12 bits of resolution.
        time.sleep(0.1)

        tmp_bytes = self.bus.read_i2c_block_data(self._address, self._temp_reg, self._temp_reg_len)

        tmp = float((tmp_bytes[0] * 256) + tmp_bytes[1]) / 256

        self._threadlock.release()

        return tmp
