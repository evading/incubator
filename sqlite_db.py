from __future__ import print_function
import sqlite3
import logging
import sys

class Database(object):

    def __init__(self, db_file):
        self._table_name = 'temperatures'
        self._database = db_file
        self.connection = None
        try:
            self.connection = sqlite3.connect(self._database)
            self.c = self.connection.cursor()

            self.c.execute('''CREATE TABLE IF NOT EXISTS {} (
                              log_id      integer PRIMARY KEY,
                              timestamp   text    NOT NULL,
                              temperature float   NOT
                              NULL)'''.format(self._table_name))
        except:
            logging.info("Could not init")

    def insert_temperature(self, temp):
        try:
            with self.connection:
                self.c.execute('''INSERT INTO {}
                                  ( timestamp, temperature)
                                  VALUES
                                  ( CURRENT_TIMESTAMP, {})'''
                                  .format(self._table_name, str(temp)))
        except:
            logging.info("Could not insert")
