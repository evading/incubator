import json
import requests
from requests.auth import HTTPDigestAuth

url = "http://192.168.1.4/vapix/axgpio"

state_true = u'{"axgpio:SetPort": {"Port": [ {"PortId": "8", "Active": true } ] } }'
state_false = u'{"axgpio:SetPort": {"Port": [ {"PortId": "8", "Active": false } ] } }'

headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "0107158c-2207-4275-2f22-263bb6973b58"
    }

response = requests.request("POST", url, data=state_false, headers=headers, auth=HTTPDigestAuth('root', 'pass'))

print(response.text)
