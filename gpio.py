import RPi.GPIO as GPIO
import smbus
import time

nexa0_pin = 17
nexa1_pin = 21
servo_pin = 18
light_pin = 22
stroke_mid = 7.5
stroke_min = 9
stroke_max = 4

GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
GPIO.setwarnings(False)
GPIO.setup(nexa0_pin, GPIO.OUT)
GPIO.setup(nexa1_pin, GPIO.OUT)
GPIO.setup(servo_pin, GPIO.OUT)
GPIO.setup(light_pin, GPIO.IN)
pwm = GPIO.PWM(servo_pin, 50)

GPIO.output(nexa0_pin, GPIO.HIGH)
GPIO.output(nexa1_pin, GPIO.HIGH)
pwm.start(stroke_mid)
time.sleep(5)

print("Here we go! Press CTRL+C to exit")
try:
    while 1:
            GPIO.output(nexa0_pin, GPIO.LOW)
            GPIO.output(nexa1_pin, GPIO.HIGH)
            pwm.ChangeDutyCycle(stroke_min)
            print GPIO.input(light_pin)
            time.sleep(10)
            GPIO.output(nexa0_pin, GPIO.HIGH)
            GPIO.output(nexa1_pin, GPIO.LOW)
            pwm.ChangeDutyCycle(stroke_max)
            print GPIO.input(light_pin)
            time.sleep(10)
except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
    pwm.stop() # stop PWM
    GPIO.cleanup() # cleanup all GPIO

