import RPi.GPIO as GPIO
import time
import logging

_nexa_sleep_time = 4
_nexa_err_sleep_time = 2 * _nexa_sleep_time

class Heater(object):

    def __init__(self, ctrl_pin, detect_pin, gpio_mode = GPIO.BCM):
        
        self._ctrl_pin = ctrl_pin
        self._detect_pin = detect_pin

        GPIO.setwarnings(False)
        self._saved_mode = GPIO.getmode()
        GPIO.setmode(gpio_mode)
        GPIO.setup(self._ctrl_pin, GPIO.OUT)
        GPIO.setup(self._detect_pin, GPIO.IN)

        # Only because there is a pullup on the Nexa thing. So this should be
        # the "Normal" state.
        self.on()

    def _print_try(self, onoff):
        logging.info("Trying to turn {}".format(onoff))

    def _print_err(self, onoff):
        logging.info("Failed to turn {}, trying again in {} s.".format(onoff,
            _nexa_err_sleep_time))

    def off(self):
        while self.is_on():
            self._print_try("off")
            GPIO.output(self._ctrl_pin, GPIO.HIGH)
            time.sleep(_nexa_sleep_time)
            if self.is_on():
                self._print_err("off")
                GPIO.output(self._ctrl_pin, GPIO.LOW)
                time.sleep(_nexa_err_sleep_time)
        logging.info("Turn off success")



    def on(self):
        while not self.is_on():
            self._print_try("on")
            GPIO.output(self._ctrl_pin, GPIO.LOW)
            time.sleep(_nexa_sleep_time)
            if not self.is_on():
                self._print_err("on")
                GPIO.output(self._ctrl_pin, GPIO.HIGH)
                time.sleep(_nexa_err_sleep_time)
        logging.info("Turn on success")

    def is_on(self):
        status = (1 == GPIO.input(self._detect_pin))
        #logging.info("Heater is {}".format("on" if status else "off"))
        return status
